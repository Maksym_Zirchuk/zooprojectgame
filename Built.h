#ifndef ZOOPROJEKT2021_BUILT_H
#define ZOOPROJEKT2021_BUILT_H
#include <iostream>
#include <vector>
#include "MessageLogger.h"

class Built {
private:
    std::vector<std::string> m_builtlist;
    static Built* s_built;
    Built();
public:
    static Built* getInstance();
    void addBuild(std::string build);
    void printBuilt();
};



#endif //ZOOPROJEKT2021_BUILT_H
