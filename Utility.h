#ifndef ZOOPROJEKT2021_UTILITY_H
#define ZOOPROJEKT2021_UTILITY_H
#include <windows.h>
#include <iostream>


// terminal output setup, drawing helpers
class Utility {
public:
    static void gotoXY(short x, short y);
    static void clearArea(int fromX, int fromY, int toX, int toY);
    static void showCursor(bool visible);
    static void setupConsole();
private:
    static void setupConsoleWindow();
    static void setupConsoleFont();
};


#endif //ZOOPROJEKT2021_UTILITY_H
