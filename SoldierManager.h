

#ifndef ZOOPROJEKT2021_SOLDIERMANAGER_H
#define ZOOPROJEKT2021_SOLDIERMANAGER_H
#include <vector>
#include "Soldier.h"

namespace ss{
    class SoldierManager {
    private:
        static std::vector <ss::Soldier* > s_soldier;
        static int s_counterSoldier;
        SoldierManager(){}

    public:
        static ss::Soldier* getSoldierById(int id);
        static std::vector<ss::Soldier* > getSoldierByType(std::string type);
        static ss::Soldier* createSoldier(std::string type);
    };

}



#endif //ZOOPROJEKT2021_SOLDIERMANAGER_H
