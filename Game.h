#ifndef ZOOPROJEKT2021_GAME_H
#define ZOOPROJEKT2021_GAME_H
#include <map>
#include <windows.h>
#include "Utility.h"
#include "MapManager.h"
#include "MessageLogger.h"
#include "age.h"
#include "SoldierManager.h"
#include "Built.h"

// core game logic, initialization and game loop, menu functions
class Game {
private:
    Built* mr_built = Built::getInstance();
    Age* m_age = new Age();
    MapManager* m_mapManager;
    bool m_running;
    int m_gold;
    int m_round = 0, m_basicaddgold = -1, m_basicaddfaith = 3;
    int m_faith = 100;
    bool m_barracks = false;


public:
    Game();
private:
    void runGame();
    void runOptionsMenu();
    bool inputIsValid(std::string input, std::vector<std::string> validOptions);
    int translateCoord(std::string input);
    void addBuilding(int type, Coord position);
    void printHelp();
    void printGold();
    void printRound();
    void printFaith();
    void printAge();
    void printSoldier();
    void printUseBuild();
};

#endif //ZOOPROJEKT2021_GAME_H
