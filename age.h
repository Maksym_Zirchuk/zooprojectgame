

#ifndef ZOOPROJEKT2021_AGE_H
#define ZOOPROJEKT2021_AGE_H
#include "future.h"
#include "modern.h"
#include "history.h"


class Age
{
private:
    AgeStatus* m_agestatus;
public:
    Age();
    ~Age();
    void SetTime();
    void NowAgeModern();
    void NowAgeFuture();

};

#endif //ZOOPROJEKT2021_AGE_H
