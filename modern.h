
#ifndef ZOOPROJEKT2021_MODERN_H
#define ZOOPROJEKT2021_MODERN_H
#include "agestatus.h"

class Modern: public AgeStatus
{
public:
    Modern();
    ~Modern() { };
    void SetTime();

};

#endif //ZOOPROJEKT2021_MODERN_H
