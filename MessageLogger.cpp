#include "MessageLogger.h"

std::deque<std::string> MessageLogger::s_messageLog = {"", "", ""};

void MessageLogger::pushMessage(std::string message) {
    s_messageLog.push_back(message);
    s_messageLog.pop_front();
    printMessageLog();
}

void MessageLogger::printMessageLog() {
    Utility::clearArea(32, 6, 90, 11);
    Utility::gotoXY(32, 4);
    std::cout << "Messages";

    Utility::gotoXY(32, 6);
    std::cout << "> " << s_messageLog.at(0);
    Utility::gotoXY(32, 8);
    std::cout << "> " << s_messageLog.at(1);
    Utility::gotoXY(32, 10);
    std::cout << "> " << s_messageLog.at(2);
}

