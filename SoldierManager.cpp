#include "SoldierManager.h"

int ss::SoldierManager::s_counterSoldier = 0;
std::vector<ss::Soldier*> ss::SoldierManager::s_soldier = {};

ss::Soldier *ss::SoldierManager::getSoldierById(int id) {
    for(Soldier* soldier:s_soldier){
        if(soldier->getId() == id){
            return soldier;
        }
    }
    return nullptr;
}

std::vector<ss::Soldier *> ss::SoldierManager::getSoldierByType(std::string type) {
    std::vector<ss::Soldier*> m_result;

    for(int i = 0; i<s_soldier.size(); i++){
        if(s_soldier.at(i)->getType() == type){
            m_result.push_back(s_soldier.at(i));
        }
    }
    return m_result;
}

ss::Soldier *ss::SoldierManager::createSoldier(std::string type) {
    Soldier* newSoldier = new Soldier(type, s_counterSoldier);
    s_counterSoldier++;
    s_soldier.push_back(newSoldier);
    return newSoldier;
}

