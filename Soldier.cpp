
#include "Soldier.h"

ss::Soldier::Soldier(std::string type, int id) {
    m_type = type;
    m_id = id;
}

int ss::Soldier::getId() {
    return this->m_id;
}

std::string ss::Soldier::getType() {
    return this->m_type;
}
