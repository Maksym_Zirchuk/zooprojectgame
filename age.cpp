
#include "age.h"

Age::Age()
{
    m_agestatus = new History();
}

Age::~Age()
{
    delete m_agestatus;
}

void Age::SetTime()
{
    m_agestatus->SetTime();
}

void Age::NowAgeModern() {
    m_agestatus = new Modern();
}

void Age::NowAgeFuture() {
    m_agestatus = new Future();
}
