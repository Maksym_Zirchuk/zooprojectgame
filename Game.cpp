#include "Game.h"


Game::Game() {
    m_running = true;
    m_mapManager = new MapManager();
    m_gold = 500;
    system("COLOR 1F");

    runGame();
}

// entry point, game loop
void Game::runGame() {


    Utility::setupConsole();
    m_mapManager->printMapTemplate();
    MessageLogger::printMessageLog();
    m_age->SetTime();

    //if(m_faith > 0 ) {
    while (m_running) {
        m_mapManager->printMap();
        printGold();
        printRound();
        printFaith();
        printAge();
        printSoldier();
        printUseBuild();
        runOptionsMenu();

        //Konec hry skrz víru
        if(m_faith > 0 ){
            m_round++;
            m_faith = (m_faith--) + m_basicaddfaith;
            m_gold = m_gold + m_basicaddgold;
        }else{
            std::string userInput;
            Utility::clearArea(1,1,128,128);
            Utility::gotoXY(35,25);
            std::cout << "End game" << std::endl;
            Utility::gotoXY(35,26);
            std::cout << "You lose for low faith";
            Utility::gotoXY(35, 29);
            std::cout << "Write any text to end the game";
            Utility::gotoXY(35,30);
            std::cout << "> ";
            Utility::showCursor(true);
            std::cin >> userInput;
            if(userInput !="╚"){
                m_running = false;}
        }

        //Změna doby
        if(m_round == 5 ) {
            m_age->NowAgeModern();
            m_age->SetTime();
            m_basicaddgold = 3;
            m_basicaddfaith = -2;
        }
        if(m_round == 15 ) {
            m_age->NowAgeFuture();
            m_age->SetTime();
            m_basicaddgold = 5;
            m_basicaddfaith = -5;
        }

    }

}

// game menu logic, main input output function
void Game::runOptionsMenu() {
    Utility::clearArea(32, 14, 90, 45);

    std::string userInput;

    // main menu
    Utility::gotoXY(32, 14);
    std::cout << "What do you want to do?";
    if(m_barracks == false){
        Utility::gotoXY(32, 16);
        std::cout << "[1] Build   [2] Print help   [4] Exit" << std::endl;
    }else{
        Utility::gotoXY(32, 16);
        std::cout << "[1] Build   [2] Print help   [3] Create a soldier  [4] Exit" << std::endl;
    }
    Utility::gotoXY(32, 18);
    std::cout << "> ";
    Utility::showCursor(true);
    std::getline(std::cin, userInput);
    //clear help menu
    Utility::clearArea(2,40,115,80);
    //
    Utility::showCursor(false);
    system("COLOR 1F");


    if (inputIsValid(userInput, {"1","2","3","4"})) {
        if (userInput == "1") {
            // building submenu
            Utility::gotoXY(32, 21);
            std::cout << "Which building?" << std::endl;
            Utility::gotoXY(32, 23);
            std::cout << "[1] Road   [2] House  [3] Farm"<< std::endl;
            Utility::gotoXY(32, 24);
            std::cout << "[4] Castle [5] Church [6] Barracks"<< std::endl;
            Utility::gotoXY(32,25);
            std::cout << "[7] Magic House" << std::endl;
            Utility::gotoXY(32, 26);
            std::cout << "> ";;
            Utility::showCursor(true);
            std::getline(std::cin, userInput);
            Utility::showCursor(false);

            if (inputIsValid(userInput, {"1", "2", "3", "4", "5", "6","7"})) {
                system("COLOR af");
                // coordinate input submenu
                std::string x;
                std::string y;

                Utility::gotoXY(32, 28);
                std::cout << "Enter x coordinate (A-Z): ";
                Utility::showCursor(true);
                std::getline(std::cin, x);
                Utility::showCursor(false);

                Utility::gotoXY(32, 30);
                std::cout << "Enter y coordinate (A-Z): ";
                Utility::showCursor(true);
                std::getline(std::cin, y);
                Utility::showCursor(false);

                int xCoord = translateCoord(x);
                int yCoord = translateCoord(y);

                if (xCoord != -1 && yCoord != -1) {
                    if (userInput == "1") {addBuilding(BT_ROAD, {xCoord, yCoord});mr_built->addBuild("Road");}
                    if (userInput == "2") {addBuilding(BT_HOUSE, {xCoord, yCoord}); m_faith = m_faith+2;mr_built->addBuild("House");}
                    if (userInput == "3") {addBuilding(BT_FARM, {xCoord, yCoord}); m_faith = m_faith+3;mr_built->addBuild("Farm");}
                    if (userInput == "4") {addBuilding(BT_CASTLE, {xCoord, yCoord}); m_faith = m_faith-10;mr_built->addBuild("Castle");}
                    if (userInput == "5") {addBuilding(BT_CHURCH, {xCoord, yCoord}); m_faith = m_faith+50;mr_built->addBuild("Church");}
                    if (userInput == "6") {addBuilding(BT_BARRACKS,{xCoord, yCoord}); m_faith = m_faith-30;m_barracks = true;mr_built->addBuild("Barracks");}
                    if (userInput == "7") {
                        addBuilding(BT_MAGICHOUSE,{xCoord, yCoord});
                        MessageLogger::pushMessage("The magic house added 50 gold and 50 faith");
                        m_gold = m_gold + 50;
                        m_faith = m_faith + 50;
                        mr_built->addBuild("Magic");
                    }
                } else {
                    system("COLOR 4f");
                    MessageLogger::pushMessage("Warning: Invalid coordinates!");
                }
            } else {
                system("COLOR cf");
                m_faith = m_faith - 15;
                MessageLogger::pushMessage("Warning: Wrong input!");
            }
        } else if (userInput == "2") {
            printHelp();
        }else if (userInput == "3" && m_barracks == true ) {
            // created soldier submenu
            system("COLOR F0");
            Utility::gotoXY(32, 21);
            std::cout << "What a soldier to create?" << std::endl;
            Utility::gotoXY(32, 23);
            std::cout << "[1] Swordsman   [2] Archer "<< std::endl;
            Utility::gotoXY(32, 25);
            std::cout << "> ";;
            Utility::showCursor(true);
            std::getline(std::cin, userInput);
            Utility::showCursor(false);

            if (inputIsValid(userInput, {"1", "2"})) {
                system("COLOR af");
                if(userInput == "1") {
                    ss::Soldier* Swordsman = ss::SoldierManager::createSoldier("Swordsman");
                    MessageLogger::pushMessage("Created 1 swordsman.");
                    m_gold = m_gold - 5;
                    m_faith = m_faith + 1 ;
                    mr_built->addBuild("Swordsman");
                }
                if(userInput == "2") {
                    ss::Soldier* Archer = ss::SoldierManager::createSoldier("Archer");
                    MessageLogger::pushMessage("Created 1 archer.");
                    m_gold = m_gold - 7 ;
                    m_faith = m_faith + 2;
                    mr_built->addBuild("Archer");
                }

            }
        }else if(userInput == "3" && m_barracks == false){
            MessageLogger::pushMessage("You don't have barracks");
        }
        else {
            m_running = false;
        }
    } else {
        system("COLOR cf");
        m_faith = m_faith - 15;
        MessageLogger::pushMessage("Warning: Wrong input!");
    }
}

// complex function for adding a building to map
void Game::addBuilding(int type, Coord position) {
    Building* newBuilding = Building::createBuilding(type, position);

    if (newBuilding->getCost() > m_gold) {
        MessageLogger::pushMessage("Not enough gold");
    } else {
        bool isOutOfBorder = m_mapManager->isOutOfBorder(newBuilding);
        bool isOverlapping = m_mapManager->isOverlapping(newBuilding);

        if (isOutOfBorder) {
            MessageLogger::pushMessage("Building is out of border!");
        } else if (isOverlapping) {
            MessageLogger::pushMessage("Buildings overlap!");
        } else {
            m_gold -= newBuilding->getCost();
            m_mapManager->pushBuilding(newBuilding);

            std::string message = newBuilding->getName() + " built successfully";
            MessageLogger::pushMessage(message);
        }
    }
}

// compares user input with vector of valid input options
bool Game::inputIsValid(std::string input, std::vector<std::string> validOptions) {
    for (std::string validOption : validOptions) {
        if (input == validOption) {
            return true;
        }
    }
    return false;
}

// translates letter coordinates to numbers, return -1 on fail
int Game::translateCoord(std::string input) {
    std::map<std::string, int> alphabetMap = {
            {"A", 0}, {"B", 1},
            {"C", 2}, {"D", 3},
            {"E", 4}, {"F", 5},
            {"G", 6}, {"H", 7},
            {"I", 8}, {"J", 9},
            {"K", 10}, {"L", 11},
            {"M", 12}, {"N", 13},
            {"O", 14}, {"P", 15},
            {"Q", 16}, {"R", 17},
            {"S", 18}, {"T", 19},
            {"U", 20}, {"V", 21},
            {"W", 22}, {"X", 23},
            {"Y", 24}, {"Z", 25},
    };

    // transform input to uppercase
    for(int i = 0; i < input.size(); i++) {
        input.at(i) = toupper(input.at(i));
    }

    // if there is a match with alphabet return coordinate number
    // no match returns -1
    if (alphabetMap.count(input) == 1) {
        return alphabetMap.at(input);
    } else {
        return -1;
    }
}

// prints gold counter
void Game::printGold() {
    Utility::clearArea(32, 1, 90, 2);
    Utility::gotoXY(32, 1);
    std::cout << "Gold " << m_gold;
}

// prints help under active game area
void Game::printHelp() {
    std::vector<Sprite> allSprites = {
            { // Uvitani
                    {"                    ╔═══════╗"},
                    {"                    ║ HELP  ║"},
                    {"                    ╚═══════╝"}
            },
            {
                    {"╔══════════════════════════════════════════════════════╗"},
                    {"║     About game                                       ║"},
                    {"╠══════════════════════════════════════════════════════╣"},
                    {"║  You build buildings and get closer to the new age.  ║"},
                    {"║  Every time has its pros and cons.                   ║"},
                    {"║  In times of history there is faith more then gold   ║"},
                    {"║  but in modern times there is more gold.             ║"},
                    {"║  You will lose if you lose faith.                    ║"},
                    {"║                                                      ║"},
                    {"║  Creators :  xpavlic6, xzirchuk, xhasek   2021-2022  ║"},
                    {"╚══════════════════════════════════════════════════════╝"}

            },
            { // Budovy a jednotky vedle
                    {"╔══════════════════════════════════════════════════════╗"" ╔══════════════════════════════════════════════════════╗"},
                    {"║     Buildings in this game                           ║"" ║     Unit in this game                                ║"},
                    {"╠══════════════════════════════════════════════════════╣"" ╠══════════════════════════════════════════════════════╣"},
                    {"║","░"," Road 1x1 [10 Gold, 0 Faith]                         ║"" ║  Swordsman =  Cost 5 Gold and add +1 Faith           ║"},
                    {"║","/", "\\","                                                    ║"" ║  Archer    =  Cost 7 Gold and add +2 Faith           ║"},
                    {"║","└", "┘"," House 2x2 [25 Gold, +2 Faith]                      ║"" ╠══════════════════════════════════════════════════════╣"},
                    {"║","/", "▲", "\\","                                                   ║"" ║ You need to have a barracks built to create units    ║"},
                    {"║","└", "─", "┘"," Farm 3x2 [50 Gold, +3 Faith]                      ║"" ╚══════════════════════════════════════════════════════╝"},
                    {"║","^","_","^","_","^","_","^","                                               ║"" ╔══════════════════════════════════════════════════════╗"},
                    {"║","|", " ", " ", " ", " ", " ", "|","                                               ║"" ║     Age in this game                                 ║"},
                    {"║","|", "_", "_", "▲", "_", "_","|"," Castle 7x3 [125 Gold, -10 Faith]              ║"" ╠══════════════════════════════════════════════════════╣"},
                    {"║"," ","+"," "," ","                                                  ║"" ║ History =  -1 Gold, +3 Faith / Round                 ║"},
                    {"║"," ","A","_"," ","                                                  ║"" ║ Modern  =  +3 Gold, -2 Faith / Round   |From 5 Round ║"},
                    {"║","/","\\","-","\\","                                                  ║"" ║ Future  =  +5 Gold, -5 Faith / Round   |From 15 Round║"},
                    {"║","|","|","_","|"," ", " Church 6x4 [150 Gold, +50 Faith]                ║"" ╚══════════════════════════════════════════════════════╝"},
                    {"║","/","-","-","\\","                                                  ║"},
                    {"║","|","_","◘","|"," Barracks 4x2[75 Gold, - 15 Faith]                ║"},
                    {"║","╔","=","╗                                                   ║"},
                    {"║","║","▓","║  Magic House 3x2[200 Gold,+50 Faith and +50 Gold] ║"},
                    {"╚══════════════════════════════════════════════════════╝"}

            },

    };

    Coord coord = {2,40};

    int startPosition = 0;
    for (int a = startPosition; a < allSprites.size(); a++) {
        coord.y+=1;
        for (int b = 0; b < allSprites[a].size(); b++) {
            Utility::gotoXY(coord.x, coord.y);
            coord.y += 1;
            for (int c = 0; c < allSprites[a][b].size(); c++) {
                std::cout << allSprites[a][b][c];
            }
        }
    }
    std::cout << std::endl;
}

void Game::printRound() {
    Utility::clearArea(42, 1, 90, 2);
    Utility::gotoXY(42, 1);
    std::cout << "Round : " << m_round;
}

void Game::printFaith() {
    Utility::clearArea(53, 1, 90, 2);
    Utility::gotoXY(53, 1);
    std::cout << "Faith : " << m_faith;
}


void Game::printAge() {
    Utility::clearArea(65, 1, 90, 2);
    Utility::gotoXY(65, 1);


}

void Game::printSoldier() {
    std::vector<ss::Soldier* > m_foundSwordsman = ss::SoldierManager::getSoldierByType("Swordsman");
    std::vector<ss::Soldier* > m_foundArcher = ss::SoldierManager::getSoldierByType("Archer");
    Utility::clearArea(3, 30, 90, 2);
    Utility::gotoXY(3, 30);
    std::cout << "Swordsman : " << m_foundSwordsman.size() << "  Archer : " << m_foundArcher.size();

}

void Game::printUseBuild() {
    mr_built->printBuilt();
}
