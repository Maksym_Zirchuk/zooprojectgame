#include "Utility.h"

// moves cursor in terminal to coordinate
void Utility::gotoXY(short x, short y) {
    COORD position = {x, y};
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
}

// clears rectangular area of terminal using spaces
void Utility::clearArea(int fromX, int fromY, int toX, int toY) {
    int rowCount = toY - fromY;
    int columnCount = toX - fromX;

    for (int row = 0; row < rowCount; row++) {
        for (int column = 0; column < columnCount; column++) {
            gotoXY(fromX+column, fromY+row);
            std::cout << " ";
        }
    }

    gotoXY(fromX, fromY);
}

// toggles terminal cursor visibility
void Utility::showCursor(bool visible) {
    CONSOLE_CURSOR_INFO info;
    info.dwSize = 25;
    info.bVisible = visible;
    SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &info);
}

// prepares console on game start
void Utility::setupConsole() {
    setupConsoleFont();
    setupConsoleWindow();
    showCursor(false);
}

// sets console output to UTF8 with 15x15 'Terminal' square font
void Utility::setupConsoleFont() {
    SetConsoleOutputCP(CP_UTF8);
    CONSOLE_FONT_INFOEX cfi;
    cfi.cbSize = sizeof(cfi);
    cfi.nFont = 0;
    cfi.dwFontSize.X = 15;
    cfi.dwFontSize.Y = 15;
    cfi.FontFamily = FF_DONTCARE;
    cfi.FontWeight = FW_REGULAR;
    std::wcscpy(cfi.FaceName, L"Terminal");
    SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);
}

// sets initial window resolution
void Utility::setupConsoleWindow() {
    HWND window = GetConsoleWindow();
    RECT ConsoleRect;
    GetWindowRect(window, &ConsoleRect);
    MoveWindow(window, ConsoleRect.left, ConsoleRect.top, 1200, 650, false);
}
