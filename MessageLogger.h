#ifndef ZOOPROJEKT2021_MESSAGELOGGER_H
#define ZOOPROJEKT2021_MESSAGELOGGER_H
#include <iostream>
#include <queue>
#include "Utility.h"


// manages 3 messages in FIFO queue
class MessageLogger {
    static std::deque<std::string> s_messageLog;
public:
    static void pushMessage(std::string message);
    static void printMessageLog();
};


#endif //ZOOPROJEKT2021_MESSAGELOGGER_H
