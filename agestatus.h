
#ifndef ZOOPROJEKT2021_AGESTATUS_H
#define ZOOPROJEKT2021_AGESTATUS_H

#include <iostream>
#include "MessageLogger.h"

class AgeStatus
{
public:
    AgeStatus() { };
    virtual ~AgeStatus() { };
    virtual void SetTime() = 0;
};



#endif //ZOOPROJEKT2021_AGESTATUS_H
