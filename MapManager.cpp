#include "MapManager.h"

MapManager::MapManager() {
    // starting coordinate for map template printing
    m_templateCoord = {2, 2};
    // starting coordinate for map printing
    m_nullCoord = {3, 3};
    // map size 26 = alphabet size
    m_mapSize = {26, 26};
}

// stores building object
void MapManager::pushBuilding(Building* building) {
    m_buildings.push_back(building);
}

// check if used coordinates of building are within map range
bool MapManager::isOutOfBorder(Building* b) {
    bool result = false;

    int posX = b->getPosition().x;
    int posY = b->getPosition().y;

    if (posX < 0 || posY < 0) {
        // negative coordinates
        result = true;
    } else {
        // building fitment relative to position and size
        int remainWidth = m_mapSize.width - posX;
        int widthFit = remainWidth - b->getSize().width;

        int remainHeight = m_mapSize.height - posY;
        int heightFit = remainHeight -  b->getSize().height;

        if (widthFit < 0 || heightFit < 0) result = true;
    }

    return result;
}

// compares all coordinates of all buildings with all coordinates of new building
// if coordinates match, there is an overlap
bool MapManager::isOverlapping(Building* newBuilding) {
    for (Building* b : m_buildings) {
        for (Coord taken : newBuilding->getUsedCoords()) {
            for (Coord needed : b->getUsedCoords()) {
                if (taken.x == needed.x && taken.y == needed.y) {
                    return true;
                }
            }
        }
    }
    return false;
}

// prints map frame with coordinates
void MapManager::printMapTemplate() {
    // frame top border
    Utility::gotoXY(m_templateCoord.x, m_templateCoord.y);
    std::cout << "┌";
    for (int i = 0; i < m_mapSize.width; i++) std::cout << "─";
    std::cout << "┐" << std::endl;

    // frame center
    Utility::gotoXY(m_templateCoord.x, m_templateCoord.y + 1);
    for (int i = 0; i < m_mapSize.height; i++) {
        Utility::gotoXY(m_templateCoord.x, m_templateCoord.y + 1 + i);

        for (int j = 0; j < m_mapSize.width + 2; j++) {
            if (j == 0 || j == m_mapSize.width + 1) std::cout << "│";
            else std::cout << " ";
        }

        std::cout << std::endl;
    }

    // frame bottom border
    Utility::gotoXY(m_templateCoord.x, m_templateCoord.y + m_mapSize.height + 1);
    std::cout << "└";
    for (int i = 0; i < m_mapSize.width; i++) std::cout << "─";
    std::cout << "┘" << std::endl;

    // alphabet coordinates
    std::string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    // x
    Utility::gotoXY(3, 1);
    std::cout << alphabet;
    // y
    for (int i = 0; i < alphabet.size(); i++) {
        Utility::gotoXY(1, 3+i);
        std::cout << alphabet.at(i);
    }
}

// redraws map with all buildings in place
void MapManager::printMap() {
    int fromX = m_nullCoord.x;
    int fromY = m_nullCoord.y;
    int toX = m_nullCoord.x+m_mapSize.width;
    int toY = m_nullCoord.y+m_mapSize.height;

    Utility::clearArea(fromX, fromY, toX, toY);

    // for all buildings write sprite character according to coordinate
    for (Building* b : m_buildings) {
        for (int row = 0; row < b->getSize().height; row++) {
            for (int column = 0; column < b->getSize().width; column++) {
                int x = m_nullCoord.x + b->getPosition().x + column;
                int y = m_nullCoord.y + b->getPosition().y + row;
                Utility::gotoXY(x, y);
                std::cout << b->getSprite().at(row).at(column);
            }
        }
    }
}

