#ifndef ZOOPROJEKT2021_BUILDING_H
#define ZOOPROJEKT2021_BUILDING_H
#include <iostream>
#include <vector>


enum BuildingType {
    BT_ROAD,
    BT_HOUSE,
    BT_FARM,
    BT_CASTLE,
    BT_CHURCH,
    BT_BARRACKS,
    BT_MAGICHOUSE
};

struct Coord {int x; int y;};

struct Size {int width; int height;};

typedef std::vector<std::vector<std::string>> Sprite;


// definition of buildings, building creation with factory method
class Building {
    int m_type;
    std::string m_name;
    Sprite m_sprite;
    Size m_size;
    int m_cost;
    Coord m_position;
    std::vector<Coord> m_usedCoords;
public:
    std::string getName();
    Sprite getSprite();
    Size getSize();
    int getCost();
    Coord getPosition();
    std::vector<Coord> getUsedCoords();
    static Building* createBuilding(int type, Coord position);
private:
    Building(int type, std::string name, Sprite sprite, Size size, int cost, Coord position);
    std::vector<Coord> generateUsedCoords(Coord position, Size size);
};
#endif //ZOOPROJEKT2021_BUILDING_H
