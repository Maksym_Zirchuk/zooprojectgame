#ifndef ZOOPROJEKT2021_MAPMANAGER_H
#define ZOOPROJEKT2021_MAPMANAGER_H
#include <iostream>
#include "Utility.h"
#include "Building.h"


// storage for building objects, map related functionality
class MapManager {
    std::vector<Building*> m_buildings;
    Coord m_nullCoord;
    Coord m_templateCoord;
    Size m_mapSize;
public:
    MapManager();
    void pushBuilding(Building* b);
    bool isOutOfBorder(Building* b);
    bool isOverlapping(Building* b);
    void printMapTemplate();
    void printMap();
};


#endif //ZOOPROJEKT2021_MAPMANAGER_H
