

#ifndef ZOOPROJEKT2021_FUTURE_H
#define ZOOPROJEKT2021_FUTURE_H
#include "agestatus.h"

class Future: public AgeStatus
{
public:
    Future();
    ~Future() { };
    void SetTime();

};

#endif //ZOOPROJEKT2021_FUTURE_H
