#ifndef ZOOPROJEKT2021_SOLDIER_H
#define ZOOPROJEKT2021_SOLDIER_H
#include <iostream>

// ss = soldier system..
namespace ss{
    class Soldier {
    private:
        std::string m_type;
        int m_id;
    public:
        Soldier(std::string type, int id);
        int getId();
        std::string getType();
    };
}




#endif //ZOOPROJEKT2021_SOLDIER_H
