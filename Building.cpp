#include "Building.h"


Building::Building(int type, std::string name, Sprite sprite, Size size, int cost, Coord position) {
    m_type = type;
    m_name = name;
    m_sprite = sprite;
    m_size = size;
    m_cost = cost;
    m_position = position;
    m_usedCoords = generateUsedCoords(position, size);

}

// generates coordinates building would use relative to position and building size
std::vector<Coord> Building::generateUsedCoords(Coord position, Size buildingSize) {
    std::vector<Coord> usedCoords;

    for (int row = 0; row < buildingSize.height; row++) {
        for (int column = 0; column < buildingSize.width; column++) {
            usedCoords.push_back({position.x + column, position.y + row});
        }
    }
    return usedCoords;
}

// factory method for buildings
Building *Building::createBuilding(int type, Coord position) {
    std::string name;
    Sprite sprite;
    Size size;
    int cost;

    switch(type) {
        case BT_ROAD:
            name = "ROAD";
            sprite = {
                    {"░"}
            };
            size = {1, 1};
            cost = 10;
            break;
        case BT_HOUSE:
            name = "HOUSE";
            sprite = {
                    {"/", "\\"},
                    {"└", "┘"}
            };
            size = {2, 2};
            cost = 25;
            break;
        case BT_FARM:
            name = "FARM";
            sprite = {
                    {"/", "▲", "\\"},
                    {"└", "─", "┘"}
            };
            size = {3, 2};
            cost = 50;
            break;
        case BT_CASTLE:
            name = "CASTLE",
                    sprite = {
                            {"^","_","^","_","^","_","^",},
                            {"|", " ", " ", " ", " ", " ", "|"},
                            {"|", "_", "_", "▲", "_", "_","|"}
                    };
            size = {7, 3};
            cost = 125;
            break;

        case BT_CHURCH:
            name = "CHURCH",
                    sprite ={
                            {" "," ","+"," "," "," "},
                            {" "," ","A","_"," "," "},
                            {" ","/","\\","-","\\"," "},
                            {" ","|","|","_","|"," "}

                    };
            size ={6,4};
            cost = 150;
            break;

        case BT_BARRACKS:
            name = "BARRACKS",
                    sprite = {
                            {"/","-","-","\\"},
                            {"|","_","◘","|"}

                    };
            size={4,2};
            cost = 75;
            break;

        case BT_MAGICHOUSE:
            name = "MAGIC HOUSE",
                    sprite = {
                            {"╔","═","╗"},
                            {"║","▓","║"}
                    };
            size={3,2};
            cost = 200;
            break;
        default:
            return nullptr;
    }

    return new Building(type, name, sprite, size, cost, position);
}

std::string Building::getName() {
    return m_name;
}

Sprite Building::getSprite() {
    return m_sprite;
}

Size Building::getSize() {
    return m_size;
}

int Building::getCost() {
    return m_cost;
}

Coord Building::getPosition() {
    return m_position;
}

std::vector<Coord> Building::getUsedCoords() {
    return m_usedCoords;
}