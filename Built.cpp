#include "Built.h"

Built::Built() {

}

Built *Built::getInstance() {
    if(s_built == nullptr){
        s_built = new Built();
    }else{
        MessageLogger::pushMessage("Tato instance už existuje");
    }
    return s_built;
}

void Built::addBuild(std::string build) {
    m_builtlist.push_back(build);
}

void Built::printBuilt() {
    int y=32;
    Utility::clearArea(3, 32, 90, 2);

    for(int i = 0; i < m_builtlist.size(); i++){
        Utility::gotoXY(3, y);
        std::cout << "How " << i << " it was created " << m_builtlist.at(i) << std::endl;
        y++;
        if(y== 38){
            y = 32;
            Utility::clearArea(3, 32, 90, 2);
            Utility::clearArea(3, 33, 90, 2);
            Utility::clearArea(3, 34, 90, 2);
            Utility::clearArea(3, 35, 90, 2);
            Utility::clearArea(3, 36, 90, 2);
            Utility::clearArea(3, 37, 90, 2);
            Utility::clearArea(3, 38, 90, 2);
        }


    }
}
